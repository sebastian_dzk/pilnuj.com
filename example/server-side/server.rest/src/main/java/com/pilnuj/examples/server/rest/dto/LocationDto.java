package com.pilnuj.examples.server.rest.dto;

public class LocationDto {
    private String imei;
    private double lat, lng;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "LocationDto{" +
                "imei='" + imei + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                '}';
    }
}
