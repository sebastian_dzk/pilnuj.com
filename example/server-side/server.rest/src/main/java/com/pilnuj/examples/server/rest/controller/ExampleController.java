package com.pilnuj.examples.server.rest.controller;

import com.pilnuj.examples.server.rest.dto.LocationDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@RestController
public class ExampleController {

    private static final Logger logger = LoggerFactory.getLogger(ExampleController.class);

    @GetMapping("/time")
    public String time() {
        String currentTime = LocalDateTime.now(ZoneId.systemDefault())
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        logger.info(currentTime);
        return currentTime;
    }

    @PostMapping("/add")
    public ResponseEntity<Void> add(@RequestBody LocationDto location) {
        logger.info(location.toString());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
