#include <SoftwareSerial.h>

static const int RXPin = 3, TXPin = 4;

SoftwareSerial softwareSerial(RXPin, TXPin);

void setup(){
  Serial.begin(9600);
  softwareSerial.begin(9600);
}

void loop(){
  while (softwareSerial.available() > 0){
    byte gpsData = softwareSerial.read();
    Serial.write(gpsData);
  }
}
