#include <SoftwareSerial.h>

static const int RXPin = 10, TXPin = 11;

SoftwareSerial mySerial(RXPin, TXPin); 

void setup() {
  // begin serial communication with Arduino
  Serial.begin(9600);
  
  // begin serial communication SIM800L 
  mySerial.begin(9600);

  Serial.println("Initializing...");
  delay(500);

  // AT command to receive SMS
  mySerial.println("AT+CNMI=2,2,0,0,0");
  delay(200);
}

void loop()
{
  updateSerial();
}

void updateSerial()
{
  delay(200);
 
  while(mySerial.available()) 
  {
    // forward Software Serial to Serial Port
    Serial.write(mySerial.read());
  }
}
