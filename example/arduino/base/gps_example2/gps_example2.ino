#include <SoftwareSerial.h>
#include <TinyGPS++.h>


static const int RXPin = 3, TXPin = 4;
SoftwareSerial softwareSerial(RXPin, TXPin);
TinyGPSPlus gps;

void setup(){
  Serial.begin(9600);
  softwareSerial.begin(9600);
}

void loop(){
  while (softwareSerial.available() > 0){
    byte gpsData = softwareSerial.read();
    
    gps.encode(gpsData);  
    if (gps.location.isUpdated())
    {
        Serial.print("lat=");
        Serial.print(gps.location.lat(), 6);
        Serial.print("  lng=");
        Serial.println(gps.location.lng(), 6);
    }
  }
}
