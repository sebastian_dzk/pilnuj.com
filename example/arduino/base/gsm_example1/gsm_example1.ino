#include <SoftwareSerial.h>

static const int RXPin = 10, TXPin = 11;

SoftwareSerial mySerial(RXPin, TXPin); 

void setup() {
  // begin serial communication with Arduino
  Serial.begin(9600);
  
  // begin serial communication SIM800L 
  mySerial.begin(9600);

  Serial.println("Initializing...");
  delay(500);

  mySerial.println("AT+CMGF=1"); // set TEXT mode
  updateSerial();
  
  // change +ZZ with country code and xxxxxxxxx with phone number to SMS
  // Poland code is +48
  mySerial.println("AT+CMGS=\"+ZZxxxxxxxxx\""); 
  updateSerial();
  
  mySerial.print("Hello world!"); // add TEXT content
  updateSerial();
  
  mySerial.write(26); // ASCII code ctrl+z
}

void loop()
{
  updateSerial();
}

void updateSerial()
{
  delay(200);
  while(mySerial.available()) 
  {
    // forward Software Serial to Serial Port
    Serial.write(mySerial.read());
  }
}
